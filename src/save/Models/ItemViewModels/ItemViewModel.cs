﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace save.Models.ItemViewModels
{
    public class ItemViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Text { get; set; }
        [Url]
        public string Url { get; set; }
        public DateTime TimeCreated { get; set; }
    }
}
