﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace save.Models
{
    public class Item
    {
        public int Id { get; set; }
        [Required]
        public string  Title { get; set; }
        public string Text { get; set; }
        [Url]
        public string Url { get; set; }
        public DateTime TimeCreated { get; set; }
        public DateTime? Deadline { get; set; }
        public bool Is_archived { get; set; }
        public string Owner_Id { get; set; }
        public User Owner { get; set; }
    }
}
