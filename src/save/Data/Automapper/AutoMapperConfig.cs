﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using save.Models;

namespace save.Data.Automapper
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            AutoMapper.Mapper.CreateMap<Item, save.Models.ItemViewModels.ItemViewModel>().ReverseMap();
        }
    }
}
