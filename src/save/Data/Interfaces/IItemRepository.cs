﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using save.Models;
using save.Models.ItemViewModels;

namespace save.Data.Interfaces
{
    public interface IItemRepository : IDisposable
    {
        ItemViewModel GetSingleByID(int id, string userID);
        IEnumerable<ItemViewModel> GetAllForUser(string userID);
        void Create(ItemViewModel ItemViewModel, string userID);
        void Delete();
        void Update();
        void Save();
    }
}
