﻿using save.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using save.Models;
using save.Data;
using save.Models.ItemViewModels;
using AutoMapper;

namespace save.Data.Repositories
{
    public class ItemRepository : IItemRepository, IDisposable
    {
        private ApplicationDbContext _context;

        public ItemRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Create(ItemViewModel IVM, string userID)
        {
            Item item = Mapper.Map<ItemViewModel, Item>(IVM);
            item.Is_archived = false;
            item.Deadline = null;
            item.TimeCreated = DateTime.Now;
            item.Owner_Id = userID;

            _context.Items.Add(item);
            Save();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public ItemViewModel GetSingleByID(int id, string userID)
        {
            Item item = _context.Items.SingleOrDefault(m => m.Id == id);
            if (item == null)
            {
                return null;
            }
            else if (item.Owner_Id != userID)
            {
                return null;
            }
            else
            {
                ItemViewModel result = Mapper.Map<Item, ItemViewModel>(item);
                return result;
            }
        }

        public IEnumerable<ItemViewModel> GetAllForUser(string userID)
        {
            List<ItemViewModel> result = new List<ItemViewModel>();
            var query = _context.Items.Where(x => x.Owner_Id == userID);

            foreach (var item in query)
            {
                ItemViewModel ivm = Mapper.Map<Item, ItemViewModel>(item);
                result.Add(ivm);
            }
            return result;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
