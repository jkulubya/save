using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using save.Models;
using AutoMapper;
using System.Collections.Generic;
using System.Security.Claims;
using System;
using Microsoft.AspNetCore.Identity;
using save.Data;
using save.Models.ItemViewModels;
using save.Data.Repositories;
using save.Data.Interfaces;
using save.Services;

namespace save.Controllers
{
    public class ItemsController : Controller
    {
        private readonly UserManager<User> _userManager;
        private IItemRepository _itemRepository;
        private readonly IUserService _currentUser;

        public ItemsController(UserManager<User> userManager, IItemRepository itemRepository, IUserService currentUser)
        {
            _currentUser = currentUser;
            _itemRepository = itemRepository;
            _userManager = userManager;
        }

        // GET: Items
        public IActionResult Index()
        {
            string userID = _userManager.GetUserId(_currentUser.GetUser());
            List<ItemViewModel> list = _itemRepository.GetAllForUser(userID).ToList();
            return View(list);
        }

        // GET: /jfgk64
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                //Add search function here
                return RedirectToAction("Index");
            }

            string userID = _userManager.GetUserId(_currentUser.GetUser());
            ItemViewModel result = _itemRepository.GetSingleByID((int)id, userID);

            if (result == null)
            {
                return NotFound();
            }           

            return View(result);
        }

        // GET: Items/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Items/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ItemViewModel IVM)
        {
            string userID = _userManager.GetUserId(_currentUser.GetUser());
            if (ModelState.IsValid)
            {
                _itemRepository.Create(IVM, userID);
                return RedirectToAction("Index");
            }
            
            return View(IVM);
        }

        // GET: Items/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                //Add search here
                return RedirectToAction("Index");
            }
            string userID = _userManager.GetUserId(_currentUser.GetUser());
            ItemViewModel result = _itemRepository.GetSingleByID((int)id, userID);

            if(result == null)
            {
                return NotFound();
            }

            return View(result);
        }

        // POST: Items/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult Edit(Item item)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Update(item);
        //        _context.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(item);
        //}

        //// GET: Items/Delete/5
        //[ActionName("Delete")]
        //public IActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    Item item = _context.Items.Single(m => m.Id == id);
        //    if (item == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(item);
        //}

        //// POST: Items/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public IActionResult DeleteConfirmed(int id)
        //{
        //    Item item = _context.Items.Single(m => m.Id == id);
        //    _context.Items.Remove(item);
        //    _context.SaveChanges();
        //    return RedirectToAction("Index");
        //}
    }
}
