﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace save.Services
{
    public interface IUserService
    {
        ClaimsPrincipal GetUser();
    }

    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor _httpContext;

        public UserService(IHttpContextAccessor HttpContext)
        {
            _httpContext = HttpContext;
        }

        public ClaimsPrincipal GetUser()
        {
            ClaimsPrincipal user = _httpContext.HttpContext.User;
            return user;
        }
    }
}
