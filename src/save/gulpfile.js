﻿/// <binding Clean='clean' />
"use strict";

var gulp = require("gulp"),
    del = require("del"),
    rimraf = require("rimraf"),
    runSequence = require("run-sequence"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    less = require("gulp-less"),
    uglify = require("gulp-uglify");

var paths = {
    webroot: "./wwwroot/"
};

paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/css/";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "dist/css/";
paths.less = paths.webroot + "css/less/**/*.less";

gulp.task("clean:js", function (cb) {
    return rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    return del(["./wwwroot/css/css/*.css", "./wwwroot/dist/css/*.css"]);
});

gulp.task("clean", ["clean:js", "clean:css"]);

gulp.task("min:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
    return gulp.src(["./wwwroot/css/css/**/*.css", "!" + paths.minCss])
        //.pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest(paths.concatCssDest));
});

gulp.task("min", ["min:js", "min:css"]);

gulp.task("less", function () {
    return gulp.src(paths.less)
        .pipe(less())
        .pipe(gulp.dest(paths.css));
});

gulp.task("default", function (cb) {
    runSequence(["clean:js","clean:css"], "less", "min", cb);
});
