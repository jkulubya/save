﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Moq;
using Xunit;
using save;
using save.Data.Interfaces;
using save.Controllers;
using Microsoft.AspNetCore.Identity;
using save.Models;
using save.Data;
using Microsoft.AspNetCore.Mvc;
using save.Tests.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using save.Models.ItemViewModels;
using System.Security.Claims;
using System.Security.Principal;
using save.Services;

namespace save.Tests.UnitTests
{
    public class ItemsControllerTests
    {
        #region ItemsController.Index
        #endregion
        #region ItemsController.Create
        #endregion
        #region ItemsController.Details
        [Fact]
        public void DetailsControllerReturnsIndexWhenNoIDIsPassed()
        {
            var mockIItemRepository = new Mock<IItemRepository>();
            var mockIUserService = new Mock<IUserService>();
            var testUserManager = MockHelpers.TestUserManager<User>();
            var controller = new ItemsController(testUserManager, mockIItemRepository.Object, mockIUserService.Object);

            var result = Assert.IsType<RedirectToActionResult>(controller.Details(null));

            Assert.Equal("Index", result.ActionName);
        }

        [Fact]
        public void DetailsControllerReturnsNotFoundErrorWhenInvalidIDIsPassed()
        {            
            var mockIItemRepository = new Mock<IItemRepository>();
            var mockIUserService = new Mock<IUserService>();
            var mockUserManager = MockHelpers.MockUserManager<User>();

            mockIItemRepository.Setup(x => x.GetSingleByID(It.IsAny<int>(), It.IsAny<string>())).Returns((ItemViewModel) null);
            mockIUserService.Setup(x => x.GetUser()).Returns(new ClaimsPrincipal());
            mockUserManager.Setup(x => x.GetUserId(It.IsAny<ClaimsPrincipal>())).Returns((string)null);

            var controller = new ItemsController(mockUserManager.Object, mockIItemRepository.Object, mockIUserService.Object);

            var result = Assert.IsType<NotFoundResult>(controller.Details(65));
        }

        [Fact]
        public void DetailsControllerReturnsDetailsViewWhenCalledWithValidID()
        {
            var mockIItemRepository = new Mock<IItemRepository>();
            var mockIUserService = new Mock<IUserService>();
            var mockUserManager = MockHelpers.MockUserManager<User>();
            ItemViewModel correctViewModel = new ItemViewModel() { Id = 1, Title="Correct View Model", Text="This is correct", TimeCreated=DateTime.Now, Url="http://www.correct.com" };

            mockIItemRepository.Setup(x => x.GetSingleByID(It.IsAny<int>(), It.IsAny<string>())).Returns((ItemViewModel) correctViewModel);
            mockIUserService.Setup(x => x.GetUser()).Returns(new ClaimsPrincipal());
            mockUserManager.Setup(x => x.GetUserId(It.IsAny<ClaimsPrincipal>())).Returns((string)null);

            var controller = new ItemsController(mockUserManager.Object, mockIItemRepository.Object, mockIUserService.Object);

            var result = Assert.IsType<ViewResult>(controller.Details(65));
            var model = Assert.IsAssignableFrom<ItemViewModel>(result.ViewData.Model);
            Assert.Equal(correctViewModel.Text, model.Text);
        }
        #endregion
        #region ItemsController.Edit
        #endregion
        #region ItemsController.Delete
        #endregion
    }
}
